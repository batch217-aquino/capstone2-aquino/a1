const mongoose= require("mongoose");
const Product = require("./Product");
const productController = require("../controllers/productController");

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: [true, "Email is Required!"]
    },
    password: {
        type: String,
        required: [true, "Password is Required!"]
    },
    isAdmin:{
        type: Boolean,
        default: false
    },
    productInfo: [{
        productId: {
            type: String,
            required: [true, "Product ID is required!"]
        },
        quantity: {
            type: Number,
            default: 1
        },
        purchasedOn:{
            type: Date,
            default: new Date()
        }
    }]



})

module.exports = mongoose.model("User", userSchema);