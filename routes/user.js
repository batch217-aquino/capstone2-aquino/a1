const express = require("express");
const router = express.Router();
const User = require("../models/User.js");
const userController = require("../controllers/userController");
const auth = require ("../auth");

router.post("/checkEmailExists", (req, res) => {
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));

})

router.post("/registerUser", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/loginUser", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


router.post("/userDetails", auth.verify, (req, res) => {
    // we can get the token by accessing req.headers.authorization
    const userData = auth.decode(req.headers.authorization)

    userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));    
})

router.get("/allUsers", auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
	userController.getAllUsers(data).then(resultFromController => res.send(resultFromController));
})

router.post("/order", auth.verify, (req, res) => {
    let data = {
        userId : auth.decode(req.headers.authorization).id,
        productId : req.body.productId

    }
	userController.order(data).then(resultFromController => res.send(resultFromController));
})


module.exports = router;